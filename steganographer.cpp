#include "steganographer.h"
#include "bitsCascader.hpp"
#include <QDebug>
#include <QFile>
#include <QDir>
#include <cstring>

int mergeColorChannel(int pub, int priv){
    priv = (priv & 0xFF) >> 6; // reduce private color to just 2 bits
    pub &= 0xFC; // zero out lowest 2 bits of public color
    int r = pub | priv; // merge
    return r;
}

QColor mergeColors(QColor pub, QColor priv)
{
    pub.setRed( mergeColorChannel(pub.red(), priv.red()));
    pub.setBlue( mergeColorChannel(pub.blue(), priv.blue()));
    pub.setGreen( mergeColorChannel(pub.green(), priv.green()));
    return pub;
}

int unmaskColor(int q)
{
    int r = (q & 3) << 6; // just keep lowest 2 bits, then amplify by 2^6
    return r;
}

QColor unmaskColor(QColor c){
    QColor q;
    q.setRed(unmaskColor(c.red()));
    q.setBlue(unmaskColor(c.blue()));
    q.setGreen(unmaskColor(c.green()));
    return q;
}

std::string flattenImagepixels(QImage img){
    std::string stretched;
    for(int y = 0; y < img.height(); y++){
        for(int x = 0; x < img.width(); x++){
            QColor qc = img.pixelColor(x, y);
            stretched += static_cast<char>(static_cast<unsigned char>(qc.red()));
            stretched += static_cast<char>(static_cast<unsigned char>(qc.green()));
            stretched += static_cast<char>(static_cast<unsigned char>(qc.blue()));
        }
    }
    return stretched;
}

std::string extractImageBits(QImage img, const int N){
    return cascadeLastNbits(flattenImagepixels(img), N);
}



/////////////////////////////////////////////////////////////////////////////////////
/// \brief ImageSteganographer::ImageSteganographer
/////////////////////////////////////////////////////////////////////////////////////

FileData FileData::createFromFile(QString fname){
    FileData dat;
    QFile file(fname);
    Q_ASSERT(file.exists());
    Q_ASSERT(file.open(QIODevice::ReadOnly));
    QByteArray barr = file.readAll();
    dat.data = std::string(barr.begin(), barr.end());
    dat.filename = QDir(fname).dirName().toStdString();
    return dat;
}

bool FileData::writeToFilePath(QString filePath){
    FileData dat;
    filePath += QDir::separator() + QString::fromStdString(filename);
    QFile file(filePath);
    Q_ASSERT(file.open(QIODevice::WriteOnly));
    file.write(data.data(), data.size());
    file.close();
    return true;
}

ImageSteganographer::ImageSteganographer(){

}

bool ImageSteganographer::canEncodeData(QImage image, const FileData &fd, int bits){
    auto px = image.width() * image.height();
    return px > int(fd.filename.size() + fd.data.size() + 6);
}

QImage ImageSteganographer::encodeImage(QImage publicImage, QImage privateImage, int /*bits */){
    qDebug() << "publicImage Size: " << publicImage.size();
    qDebug() << "privateImage Size: " << privateImage.size() << "\n";
    Q_ASSERT( privateImage.size() == publicImage.size());
    for(int x = 0; x < publicImage.width(); x++){
        for(int y = 0; y < publicImage.height(); y++){
            publicImage.setPixelColor(x, y, mergeColors(publicImage.pixelColor(x, y), privateImage.pixelColor(x, y)));
        }
    }
    return publicImage;
}


QImage ImageSteganographer::decodeImage(QImage image, int /*bits*/){
    for(int x = 0; x < image.width(); x++){
        for(int y = 0; y < image.height(); y++){
            image.setPixelColor(x, y, unmaskColor( image.pixelColor(x, y) ) );
        }
    }
    return image;
}

QImage ImageSteganographer::encodeData(QImage publicImage, const FileData &data, int bits){
    std::string flattened = flattenImagepixels(publicImage);
    std::uint32_t dataSize = data.data.size();
    std::uint16_t filenameSize = data.filename.size();

    char dSize[4];
    std::memcpy(dSize, &dataSize, 4);
    char fSize[2];
    std::memcpy(fSize, &filenameSize, 2);

    std::string z = "axsd";
    z[0] = dSize[0];
    z[1] = dSize[1];
    z[2] = dSize[2];
    z[3] = dSize[3];

    std::uint32_t rev;
    std::memcpy(&rev, z.data(), 4);

    std::string dataStream;
    dataStream += dSize[0];
    dataStream += dSize[1];
    dataStream += dSize[2];
    dataStream += dSize[3];
    dataStream += fSize[0];
    dataStream += fSize[1];
    dataStream += data.filename;
    dataStream += data.data;

    QImage rslt = publicImage;
    int k = imbueIntoLastNbitsOf(flattened, dataStream, bits);
    Q_UNUSED(k);
    //Q_ASSERT(k == int(dataSize + filenameSize + 6));

    unsigned int x = 0, y = 0, i = 0;
    unsigned int qcc = 0;
    QColor qc;
    while(i < dataStream.size()){
        if(i != 0 && (i % (publicImage.width() * 3)) == 0){
            y += 1, x = 0;
        }
        int val = static_cast<unsigned char>(dataStream[i]);
        if(qcc == 0)
            qc.setRed(val);
        else if(qcc == 1)
            qc.setGreen(val);
        else if(qcc == 2)
            qc.setBlue(val);
        if(++qcc == 3)
            qcc = 0, rslt.setPixelColor(x++, y, qc);
        i++;
    }
    return rslt;
}

FileData ImageSteganographer::decodeData(QImage publicImage, int bits){
    QImage rslt = publicImage;
    std::string dataStream = flattenImagepixels(publicImage);
    std::string items = extractImageBits(publicImage, bits);
    items = dataStream;
    int kk = items.size();
    Q_UNUSED(kk);
    Q_ASSERT(items.size() > 20);

    std::uint32_t dataSize;
    std::memcpy(&dataSize, items.data(), 4);
    Q_ASSERT(items.size() > dataSize + 4);

    std::uint16_t filenameSize;
    std::memcpy(&filenameSize, items.data() + 4, 2);  //We are at offset 6 now...
    Q_ASSERT(items.size() > dataSize + 6 + filenameSize);
    std::string filename = std::string(items.data() + 6, filenameSize);
    std::string data = std::string(items.data() + 6 + filenameSize, dataSize);

    FileData d;
    d.data = data;
    d.filename = filename;
    return d;
}










