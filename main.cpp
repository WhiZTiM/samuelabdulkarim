#include "mainwindow.h"
#include <QApplication>
#include "steganographer.h"

int main(int argc, char *argv[])
{
    qRegisterMetaType<FileData>();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
