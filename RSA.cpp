/* //////////////////////////////////////////////////////
 * ///////////// (C) nHub Nigeria Limited  //////////////
 * ///// Author: Ibrahim Timothy Onogu, August 2017 /////
 * //////////////////////////////////////////////////////
 **/

#include <memory>
#include <fstream>
#include <iostream>

#include "RSA.hpp"
#include "mbed/mbedtls/pk.h"
#include "mbed/mbedtls/rsa.h"
#include "mbed/mbedtls/entropy.h"
#include "mbed/mbedtls/ctr_drbg.h"

template<typename T>
T* remove_const(const T* t){ return const_cast<T*>(t); }

class RSA::RSAImpl{
public:

    RSAImpl(PKCSType type) : m_pkcsType(type){
        //Initialize Entropy
        mbedtls_entropy_init(&m_entropy_context);

        //Initialize Random number generator
        mbedtls_ctr_drbg_init(&m_ctr_drbg_context);

        //Seed random number generator
        if(0 != mbedtls_ctr_drbg_seed(&m_ctr_drbg_context, &mbedtls_entropy_func,
                                      &m_entropy_context, reinterpret_cast<unsigned char*>(&m_pers[0]), m_pers.size()))
        {
            std::cerr << "Failed to initialize and seed Pseudorandom number generator\n";
            m_is_fully_initialized = false;
        }

        //Initialize PK context
        mbedtls_pk_init(&m_pk_context);

        //Set PK up...
        mbedtls_pk_setup(&m_pk_context, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));

        m_keyType = RSA::KeyType::PublicPrivate;
    }

    RSAImpl(unsigned int bits, int exponent, PKCSType type) : RSAImpl(type) {
        //Generate a default key
        if(!generate_new_key(bits, exponent)){
            std::cerr << "Failed to generate RSA key of " << bits << " bits, and " << exponent << " exponent\n";
            m_is_fully_initialized = false;
        }
    }

    RSAImpl(const std::string& str, RSA::KeyType ktype){

        //Initialize Entropy
        mbedtls_entropy_init(&m_entropy_context);

        //Initialize Random number generator
        mbedtls_ctr_drbg_init(&m_ctr_drbg_context);

        //Seed random number generator
        if(0 != mbedtls_ctr_drbg_seed(&m_ctr_drbg_context, &mbedtls_entropy_func,
                                      &m_entropy_context, reinterpret_cast<unsigned char*>(&m_pers[0]), m_pers.size()))
        {
            std::cerr << "Failed to initialize and seed Pseudorandom number generator\n";
            m_is_fully_initialized = false;
        }

        //Initialize PK context
        mbedtls_pk_init(&m_pk_context);

        m_keyType = ktype;
        if(ktype == RSA::KeyType::Private){
            if(0 != mbedtls_pk_parse_key(&m_pk_context,
                                         reinterpret_cast<const unsigned char*>(str.c_str()),
                                         str.size()+1, nullptr, 0)){
                std::cerr << "Failed to parse private RSA key of:\n\"" << str << "\"\n\n";
                m_is_fully_initialized = false;
            }
        }
        else if(ktype == RSA::KeyType::Public){
            auto res = mbedtls_pk_parse_public_key(&m_pk_context,
                                         reinterpret_cast<const unsigned char*>(str.c_str()),
                                         str.size()+1);
            if(res != 0){
                std::cerr << "Failed to parse public RSA key of:\n\"" << str << "\"\n\n";
                m_is_fully_initialized = false;
            }
        }
        else{
            std::cerr << "The given key cannot be both public and private";
            m_is_fully_initialized = false;
        }
    }

    bool generate_new_key(unsigned int bits, int exponent){
        auto s = mbedtls_rsa_gen_key(mbedtls_pk_rsa(m_pk_context), &mbedtls_ctr_drbg_random, &m_ctr_drbg_context, bits, exponent);
        return s == 0;
    }

    std::string encrypt(const std::string& str) const {
        auto olen = str.size() * 16 > 512 ? str.size() * 16 : 512;
        std::string output(olen, char());
        std::size_t output_length = 0;
        auto rtn = mbedtls_pk_encrypt(remove_const(&m_pk_context), reinterpret_cast<const unsigned char*>(str.c_str()), str.size(),
                                      reinterpret_cast<unsigned char*>(&output[0]), &output_length, output.size(),
                                      mbedtls_ctr_drbg_random, remove_const(&m_ctr_drbg_context));
        if(rtn != 0){
            std::cerr << "Encryption failed :-(\n";
            return std::string();
        }
        output.resize(output_length);
        return output;
    }

    std::string decrypt(const std::string& str) const {
        auto olen = str.size() * 16 > 512 ? str.size() * 16 : 512;
        std::string output(olen, char());
        std::size_t output_length = 0;
        auto rtn = mbedtls_pk_decrypt(remove_const(&m_pk_context), reinterpret_cast<const unsigned char*>(str.c_str()), str.size(),
                                      reinterpret_cast<unsigned char*>(&output[0]), &output_length, output.size(),
                                      mbedtls_ctr_drbg_random, remove_const(&m_ctr_drbg_context));
        if(rtn != 0){
            std::cerr << "Decryption failed :-(\n";
            return std::string();
        }
        output.resize(output_length);
        return output;
    }

    std::string get_private_PEM() const {
        std::string buffer(16000, '\0');
        if(0 != mbedtls_pk_write_key_pem(remove_const(&m_pk_context), reinterpret_cast<unsigned char*>(&buffer[0]), buffer.size()))
            return std::string();
        buffer.resize(buffer.find('\0'));
        return buffer;
    }

    std::string get_public_PEM() const {
        std::string buffer(16000, '\0');
        if(0 != mbedtls_pk_write_pubkey_pem(remove_const(&m_pk_context), reinterpret_cast<unsigned char*>(&buffer[0]), buffer.size()))
            return std::string();
        buffer.resize(buffer.find('\0'));
        return buffer;
    }

    ~RSAImpl(){
        mbedtls_pk_free(&m_pk_context);
        mbedtls_entropy_free(&m_entropy_context);
        mbedtls_ctr_drbg_free(&m_ctr_drbg_context);
    }

    bool is_initialized() const { return m_is_fully_initialized; }
    RSA::KeyType key_type() const { return m_keyType; }

private:
    mbedtls_ctr_drbg_context m_ctr_drbg_context;
    mbedtls_entropy_context m_entropy_context;
    mbedtls_pk_context m_pk_context;
    std::string m_pers = "rsa_encrypt";
    PKCSType m_pkcsType = PKCSType::V15;
    RSA::KeyType m_keyType = RSA::KeyType::PublicPrivate;

    bool m_is_fully_initialized = true;
};







//////////////// RSA Implementation /////////////////////////////







RSA::RSA(unsigned int bits, int exponent, PKCSType type)
    : pImpl(std::unique_ptr<RSAImpl>(new RSAImpl(bits, exponent, type)))
{
    if(!pImpl->is_initialized()){
        throw std::runtime_error("Unable create RSA");
    }
}

RSA::RSA(RSA&& other)
    : pImpl(std::move(other.pImpl))
{
    //
}

RSA& RSA::operator = (RSA&& other)
{
    pImpl = std::move(other.pImpl);
    return *this;
}

RSA::RSA(const std::string& str, KeyType keyType)
    : pImpl(std::unique_ptr<RSAImpl>(new RSAImpl(str, keyType)))
{
    if(!pImpl->is_initialized()){
        throw std::runtime_error("Unable create RSA");
    }
}

bool RSA::generate_new_key(unsigned int bits, int exponent){
    return pImpl->generate_new_key(bits, exponent);
}

std::string RSA::get_private_PEM() const {
    return pImpl->get_private_PEM();
}

std::string RSA::get_public_PEM() const {
    return pImpl->get_public_PEM();
}

std::string RSA::decrypt(const std::string& str) const{
    return pImpl->decrypt(str);
}

std::string RSA::encrypt(const std::string& str) const{
    return pImpl->encrypt(str);
}

RSA::KeyType RSA::key_type() const{
    return pImpl->key_type();
}

RSA RSA::from_PEM_string(const std::string &str, KeyType keyType){
    return std::move(RSA(str, keyType));
}

RSA RSA::from_PEM_file(const std::string& fileName, KeyType keyType){
    std::ifstream stream(fileName);
    std::string data, line;
    while(std::getline(stream, line)){
        data += line;
        data += '\n';
    }
    if(data.empty()){
        throw std::runtime_error("Unable to read file, or file empty");
    }
    data.pop_back(); //pop the last newline.
    return RSA::from_PEM_string(data, keyType);
}

RSA::~RSA(){
    // nothing... RAII
}
