#ifndef STEGANOGRAPHER_H
#define STEGANOGRAPHER_H

#include <QImage>

struct FileData{
    std::string filename;
    std::string data;
    static FileData createFromFile(QString fname);
    bool writeToFilePath(QString path);
};

Q_DECLARE_METATYPE(FileData)

class ImageSteganographer
{
public:
    ImageSteganographer();
    static bool canEncodeData(QImage image, const FileData& fd, int bits);
    static QImage encodeImage(QImage publicImage, QImage privateImage, int bits);
    static QImage decodeImage(QImage image, int bits);
    static QImage encodeData(QImage publicImage, const FileData &data, int bits);
    static FileData decodeData(QImage publicImage, int bits);
};

#endif // STEGANOGRAPHER_H
