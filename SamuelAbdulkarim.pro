#-------------------------------------------------
#
# Project created by QtCreator 2017-09-19T22:41:45
#
#-------------------------------------------------

QT       += core gui

CONFIG += link_pkgconfig
PKGCONFIG = GraphicsMagick++

QMAKE_CXXFLAGS += -std=c++14 -Wextra #`GraphicsMagick++-config`
#QMAKE_LFLAGS += `GraphicsMagick++-config`

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SamuelAbdulkarim
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        RSA.cpp \
        mainwindow.cpp \
        statisticsmodel.cpp \
        steganographer.cpp \
        mbed/src/arc4.c \
        mbed/src/aesni.c \
        mbed/src/sha1.c \
        mbed/src/entropy.c \
        mbed/src/hmac_drbg.c \
        mbed/src/x509_csr.c \
        mbed/src/des.c \
        mbed/src/asn1parse.c \
        mbed/src/padlock.c \
        mbed/src/pkparse.c \
        mbed/src/cmac.c \
        mbed/src/asn1write.c \
        mbed/src/bignum.c \
        mbed/src/ssl_srv.c \
        mbed/src/x509_create.c \
        mbed/src/entropy_poll.c \
        mbed/src/x509write_csr.c \
        mbed/src/version.c \
        mbed/src/ecdsa.c \
        mbed/src/md_wrap.c \
        mbed/src/havege.c \
        mbed/src/oid.c \
        mbed/src/rsa.c \
        mbed/src/ssl_cache.c \
        mbed/src/sha256.c \
        mbed/src/ecdh.c \
        mbed/src/md2.c \
        mbed/src/certs.c \
        mbed/src/timing.c \
        mbed/src/ssl_ciphersuites.c \
        mbed/src/ecp.c \
        mbed/src/ctr_drbg.c \
        mbed/src/gcm.c \
        mbed/src/pkwrite.c \
        mbed/src/blowfish.c \
        mbed/src/md5.c \
        mbed/src/pkcs11.c \
        mbed/src/pem.c \
        mbed/src/pk.c \
        mbed/src/error.c \
        mbed/src/x509.c \
        mbed/src/base64.c \
        mbed/src/memory_buffer_alloc.c \
        mbed/src/md4.c \
        mbed/src/ssl_ticket.c \
        mbed/src/net_sockets.c \
        mbed/src/ssl_cookie.c \
        mbed/src/pk_wrap.c \
        mbed/src/aes.c \
        mbed/src/sha512.c \
        mbed/src/version_features.c \
        mbed/src/pkcs5.c \
        mbed/src/ecjpake.c \
        mbed/src/ssl_tls.c \
        mbed/src/x509write_crt.c \
        mbed/src/debug.c \
        mbed/src/xtea.c \
        mbed/src/ssl_cli.c \
        mbed/src/dhm.c \
        mbed/src/ecp_curves.c \
        mbed/src/platform.c \
        mbed/src/x509_crl.c \
        mbed/src/cipher.c \
        mbed/src/pkcs12.c \
        mbed/src/md.c \
        mbed/src/ripemd160.c \
        mbed/src/cipher_wrap.c \
        mbed/src/x509_crt.c \
        mbed/src/camellia.c \
        mbed/src/ccm.c \
        mbed/src/threading.c

HEADERS  += \
        RSA.hpp \
        mainwindow.h \
        statisticsmodel.h \
        steganographer.h \
        bitsCascader.hpp \
        mbed/mbedtls/md2.h \
        mbed/mbedtls/timing.h \
        mbed/mbedtls/pem.h \
        mbed/mbedtls/x509.h \
        mbed/mbedtls/havege.h \
        mbed/mbedtls/memory_buffer_alloc.h \
        mbed/mbedtls/net_sockets.h \
        mbed/mbedtls/ssl_ciphersuites.h \
        mbed/mbedtls/ecjpake.h \
        mbed/mbedtls/md4.h \
        mbed/mbedtls/blowfish.h \
        mbed/mbedtls/platform.h \
        mbed/mbedtls/sha256.h \
        mbed/mbedtls/ssl_ticket.h \
        mbed/mbedtls/base64.h \
        mbed/mbedtls/x509_crl.h \
        mbed/mbedtls/config.h \
        mbed/mbedtls/entropy_poll.h \
        mbed/mbedtls/ssl_cookie.h \
        mbed/mbedtls/bn_mul.h \
        mbed/mbedtls/compat-1.3.h \
        mbed/mbedtls/sha512.h \
        mbed/mbedtls/bignum.h \
        mbed/mbedtls/x509_csr.h \
        mbed/mbedtls/net.h \
        mbed/mbedtls/des.h \
        mbed/mbedtls/xtea.h \
        mbed/mbedtls/asn1.h \
        mbed/mbedtls/ripemd160.h \
        mbed/mbedtls/aes.h \
        mbed/mbedtls/x509_crt.h \
        mbed/mbedtls/camellia.h \
        mbed/mbedtls/ccm.h \
        mbed/mbedtls/entropy.h \
        mbed/mbedtls/pkcs11.h \
        mbed/mbedtls/aesni.h \
        mbed/mbedtls/asn1write.h \
        mbed/mbedtls/ecdsa.h \
        mbed/mbedtls/pkcs12.h \
        mbed/mbedtls/ctr_drbg.h \
        mbed/mbedtls/gcm.h \
        mbed/mbedtls/oid.h \
        mbed/mbedtls/threading.h \
        mbed/mbedtls/version.h \
        mbed/mbedtls/md.h \
        mbed/mbedtls/dhm.h \
        mbed/mbedtls/md_internal.h \
        mbed/mbedtls/pk_internal.h \
        mbed/mbedtls/check_config.h \
        mbed/mbedtls/ssl_internal.h \
        mbed/mbedtls/ecp.h \
        mbed/mbedtls/md5.h \
        mbed/mbedtls/arc4.h \
        mbed/mbedtls/cmac.h \
        mbed/mbedtls/pkcs5.h \
        mbed/mbedtls/debug.h \
        mbed/mbedtls/ssl_cache.h \
        mbed/mbedtls/hmac_drbg.h \
        mbed/mbedtls/cipher.h \
        mbed/mbedtls/rsa.h \
        mbed/mbedtls/ssl.h \
        mbed/mbedtls/pk.h \
        mbed/mbedtls/certs.h \
        mbed/mbedtls/ecp_internal.h \
        mbed/mbedtls/error.h \
        mbed/mbedtls/platform_time.h \
        mbed/mbedtls/padlock.h \
        mbed/mbedtls/cipher_internal.h \
        mbed/mbedtls/ecdh.h \
        mbed/mbedtls/sha1.h

INCLUDEPATH += mbed

FORMS    += mainwindow.ui
