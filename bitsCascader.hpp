#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

inline void print_bits_forward1(unsigned char c){
    for(unsigned i = 1; i <= 8u; i++){
        auto k = c;
        k >>= (8 - i);
        k &= 1u;
        int val = k ? 1 : 0;
        std::cout << val << (i == 4 ? " " : "");
    }
    std::cout << " -- ";
}

inline void print_bits_forward(unsigned char c){
    unsigned char k = 128;
    for(unsigned i = 0; i < 8; i++){
        int val = (c & k) ? 1 : 0;
        k >>= 1;
        std::cout << val << (i == 3 ? " " : "");
    }
    std::cout << " -- ";
}

inline void print_bits_backward(unsigned char c){
    unsigned char k = 1;
    for(unsigned i = 0; i < 8; i++){
        int val = (c & k) ? 1 : 0;
        k <<= 1;
        std::cout << val << (i == 3 ? " " : "");
    }
    std::cout << " -- ";
}

inline std::string cascadeFirstNbits(const std::string& data, unsigned short N){
    if(N < 1 || N > 8)
        throw std::runtime_error("Stupid bozo");

    struct {
        unsigned char seen = 0;
        unsigned char bits = 0;
    } bits;

    std::string result;
    auto index = 0u;
    unsigned char chunk = 128;
    N = 128 >> (N - 1);

    while(index < data.size()){
        unsigned char c = data[index];
        unsigned char mm = (c & chunk) ? 128u : 0u;
        bits.bits |= mm >> bits.seen;
        bits.seen++;
        chunk >>= 1;
        if(chunk < N)
            index++, chunk = 128;
        if(bits.seen == 8){
            result += bits.bits;
            bits.seen = 0;
            bits.bits = 0;
        }
    }
    return result;
}

inline std::string cascadeLastNbits(const std::string& data, unsigned short N){
    if(N < 1 || N > 8)
        throw std::runtime_error("Stupid bozo");
    struct {
        unsigned char seen = 0;
        unsigned char bits = 0;
    } bits;

    std::string result;
    auto index = 0u;
    unsigned char chunk = 1 << (N - 1);
    N = chunk;
    while(index < data.size()){
        unsigned char c = data[index];
        unsigned char mm = (c & chunk) ? 128u : 0u;
        bits.bits |= mm >> bits.seen++;
        chunk >>= 1;
        if(chunk == 0)
            index++, chunk = N;
        if(bits.seen == 8){
            result += bits.bits;
            bits.seen = 0;
            bits.bits = 0;
        }
    }
    return result;
}

inline int imbueIntoFirstNbitsOf(std::string& data, const std::string& payload, unsigned short N){
    if(N < 1 || N > 8)
        throw std::runtime_error("Stupid bozo");
    if(payload.size() * 8 > data.size() * N)
        throw std::runtime_error("This wouldn't fit...");
    auto di = 0u;
    auto pi = 0u;
    auto dbi = 0u;
    auto pbi = 0u;
    while(di < data.size() && pi < payload.size()){
        auto& c = data[di];
        auto bit = (payload[pi] & (128 >> pbi)) ? 1 : 0;
        c ^= (-bit ^ c) & (1 << dbi);
        if(pbi++ == 7)
            pbi = 0, pi++;
        if(dbi++ == N)
            dbi = 0, di++;
    }
    return di;
}

inline int imbueIntoLastNbitsOf(std::string& data, const std::string& payload, unsigned short N){
    if(N < 1 || N > 8)
        throw std::runtime_error("Stupid bozo");
    if(payload.size() * 8 > data.size() * N)
        throw std::runtime_error("This wouldn't fit");
    auto di = 0u;
    auto pi = 0u;
    signed short dbi = N - 1;
    auto pbi = 0;
    while(di < data.size() && pi < payload.size()){
        auto& c = data[di];
        auto bit = (payload[pi] & (128 >> pbi)) ? 1 : 0;
        c ^= (-bit ^ c) & (1 << dbi);
        if(pbi++ == 7)
            pbi = 0, pi++;
        if(dbi-- <= 0)
            dbi = N-1, di++;
    }
    return di;
}

inline void print_bits_forward(const std::string& str){
    for(char c : str) print_bits_forward(c);
    std::cout << "\n\n";
}
