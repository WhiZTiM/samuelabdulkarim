#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "statisticsmodel.h"

QT_BEGIN_NAMESPACE
class QLabel;
QT_END_NAMESPACE

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openActionTriggered();
    void saveActionTriggered();
    void encodePushButtonClicked();
    void decodePushButtonClicked();
    void encodeDataPushButtonClicked();
    void decodeDataPushButtonClicked();
    void stegBitsChanged(QString);
    void rsaBitsChanged(QString);

private:
    Ui::MainWindow *ui;
    QImage publicImage;
    QImage privateImage;

    QLabel* publicImageLabel;
    QLabel* privateImageLabel;
    StatisticsModel* statsModel;
    int stegBits = 2;
    int rsaBits = 1024;
};

#endif // MAINWINDOW_H
