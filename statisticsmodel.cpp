#include "statisticsmodel.h"
#include <algorithm>

void StatisticsModel::addOrSetItem(QString key, QString value){
    beginRemoveRows(QModelIndex(), 0, m_items.size());
    endRemoveRows();
    m_items[key] = value;
    beginInsertRows(QModelIndex(), 0, m_items.size());
    endInsertRows();
}

QVariant StatisticsModel::data(const QModelIndex &index, int role) const{
    if(role != Qt::DisplayRole) return QVariant();
    if(index.column() >= 2) return {};
    if(index.row() >= m_items.size()) return {};
    auto it = m_items.begin();
    std::advance(it, index.row());
    if(index.column() == 0)
        return QVariant(it.key());
    else
        return QVariant(it.value());
}

void StatisticsModel::removeItem(QString key){
    beginRemoveRows(QModelIndex(), 0, m_items.size());
    endRemoveRows();
    m_items.remove(key);
    beginInsertRows(QModelIndex(), 0, m_items.size());
    endInsertRows();
}

int StatisticsModel::rowCount(const QModelIndex&) const{
    return m_items.size();
}

int StatisticsModel::columnCount(const QModelIndex&) const{
    return 2;
}
