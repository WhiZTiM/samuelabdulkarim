#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QTimer>
#include <QDebug>
#include <QSplitter>
#include <QFile>
#include <QMessageBox>
#include <QPicture>
#include <QPixmap>
#include <QTime>
#include <QFileDialog>
#include <QLayout>
#include <QLocale>
#include "steganographer.h"

int calculateMaximumImageSteganographgy(const QImage& img, int bits){
    return (img.size().width() * img.size().height() * bits * (img.depth()/8));
}

int calculateMaxDataRSA(int bits){
    return bits / 8 - 42;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->statsTabWidget->setCurrentIndex(0);
    ui->statsTabWidget->setTabEnabled(1, false);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::openActionTriggered);
    connect(ui->encodePushButton, &QPushButton::clicked, this, &MainWindow::encodePushButtonClicked);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::saveActionTriggered);
    connect(ui->actionExit, &QAction::triggered, qApp, &QApplication::exit);
    connect(ui->decodePushButton, &QPushButton::clicked, this, &MainWindow::decodePushButtonClicked);
    connect(ui->stegBitsComboBox, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged),
            this, &MainWindow::stegBitsChanged);
    connect(ui->rsaBitsComboBox, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged),
            this, &MainWindow::rsaBitsChanged);

    connect(ui->encodeDataPushButton, &QPushButton::clicked, this, &MainWindow::encodeDataPushButtonClicked);
    connect(ui->decodeDataPushButton, &QPushButton::clicked, this, &MainWindow::decodeDataPushButtonClicked);

    ui->logTextEdit->setReadOnly(true);

    ui->viewWidget->setLayout(new QHBoxLayout());

    publicImageLabel = new QLabel("Please open a Primary Image");
    privateImageLabel = new QLabel("Results of actions performed on the Primary image will be shown here");
    QSplitter* splitter = new QSplitter(Qt::Horizontal);
    ui->viewWidget->layout()->addWidget(splitter);
    splitter->addWidget(publicImageLabel);
    splitter->addWidget(privateImageLabel);

    statsModel = new StatisticsModel();
    ui->tableView->setModel( statsModel );
    ui->tableView->horizontalHeader()->hide();
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setColumnWidth(0, 360);
    ui->tableView->setColumnWidth(1, 160);
    ui->stegBitsComboBox->setCurrentIndex(1);
    setWindowState(Qt::WindowMaximized);
}

void MainWindow::openActionTriggered(){
    QString img = QFileDialog::getOpenFileName(this, tr("Open Image"), ".", "Images (*.png *.jpg *.jpeg *.bmp)");
    if(img.isEmpty()) return;
    QTime qt;
    qt.start();
    publicImage = QImage(img);
    QPixmap p = QPixmap::fromImage(publicImage);
    auto iSize = p.size();
    auto size = p.size() * (p.depth() / 8);
    p = p.scaled(p.size() / 4);
    publicImageLabel->setPixmap(p);
    publicImageLabel->resize(p.size() / 9);

    QString imgLoadingTime = QString::number(qt.elapsed()) + "ms";
    QString imgMemory = QLocale(QLocale::English).toString( size.width() * size.height() ) + " Bytes";
    QString imgDimensions = QString::number(iSize.width()) + " x " +  QString::number(iSize.height());
    ui->logTextEdit->append(">> Opening image took " + imgLoadingTime);
    ui->logTextEdit->append(">> Image memory " + imgMemory);
    ui->logTextEdit->append(">> Image dimensions (w*h): " + imgDimensions);
    ui->statsTabWidget->setTabEnabled(1, true);

    statsModel->addOrSetItem("Primary Image Loading Time", imgLoadingTime);
    statsModel->addOrSetItem("Primary Image Memory Consumption", imgMemory);
    statsModel->addOrSetItem("Primary Image Dimensions", imgDimensions);

    QTimer::singleShot(2000, [this](){ ui->statsTabWidget->setCurrentIndex(1); });
}

void MainWindow::saveActionTriggered(){
    if(privateImage.isNull()){
        ui->logTextEdit->append(">> Unable to save image, no Image processed");
        return;
    }
    QString img = QFileDialog::getSaveFileName(this, tr("Save Image"), ".", "BMP (*.bmp)");
    if(img.isEmpty()) return;
    privateImage.save(img, nullptr, 100);
    ui->logTextEdit->append(">> Successfully saved image");
}

void MainWindow::encodePushButtonClicked(){
    if(publicImage.isNull()){
        ui->logTextEdit->append(">> Unable to Encode Image, no Image loaded");
        return;
    }
    QMessageBox::information(this, "Select Image to hide...",
                             "Please Select an image of the same size to mask...",
                             QMessageBox::Ok);
    ui->logTextEdit->append(">> ...Applying" + QString::number(stegBits) + " bits per channel Steganography");
    QString img = QFileDialog::getOpenFileName(this, tr("Open Image"), ".", "Images (*.png *.jpg *.jpeg *.bmp)");
    if(img.isEmpty()) return;
    QTime qt; qt.start();
    QImage image(img);

    QString imgTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";

    ui->logTextEdit->append(QString(">> Opening image took ") + imgTime);
    qt.restart();

    privateImage = ImageSteganographer::encodeImage(publicImage, image, stegBits);
    QPixmap p = QPixmap::fromImage(privateImage);
    p = p.scaled(p.size() / 4);
    privateImageLabel->setPixmap(p);
    privateImageLabel->resize(p.size() / 9);
    QString stegTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";
    ui->logTextEdit->append(QString(">> Steganography image masking took ") + stegTime);
    ui->logTextEdit->append(QString(">> Opening image took ") + QString::number(qt.elapsed()) + QString("ms"));

    statsModel->addOrSetItem("Secondary Image Loading Time", imgTime);
    statsModel->addOrSetItem("Steganography Image Encode: Processing Time", stegTime);
}

void MainWindow::decodePushButtonClicked(){
    if(publicImage.isNull()){
        ui->logTextEdit->append(">> Unable to Decode Image, no Image loaded");
        return;
    }
    ui->logTextEdit->append(QString("Decoding image... "));
    QTime qt; qt.start();

    privateImage = ImageSteganographer::decodeImage(publicImage, stegBits);
    QPixmap p = QPixmap::fromImage(privateImage);
    p = p.scaled(p.size() / 4);
    privateImageLabel->setPixmap(p);
    privateImageLabel->resize(p.size() / 9);
    QString stegTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";
    ui->logTextEdit->append(QString(">> Steganography masking took ") + stegTime);
    ui->logTextEdit->append(QString(">> Opening image took ") + QString::number(qt.elapsed()) + QString("ms"));

    statsModel->addOrSetItem("Steganography Decode: Processing Time", stegTime);
    statsModel->removeItem("Secondary Image Loading Time");
    statsModel->removeItem("Steganography Image Encode: Processing Time");
}

////////////////////////////////////////////////////////////
/// \brief MainWindow::stegBitsChanged
/// \param str

void MainWindow::encodeDataPushButtonClicked(){
    if(publicImage.isNull()){
        ui->logTextEdit->append(">> Unable to Encode Image, no Image loaded");
        return;
    }
    QMessageBox::information(this, "Select Data to hide...",
                             "Please Select a small data file to hide",
                             QMessageBox::Ok);
    ui->logTextEdit->append(">> ...Applying" + QString::number(stegBits) + " bits per channel Steganography");
    QString dataPath = QFileDialog::getOpenFileName(this, tr("Open Data"), ".", "Any file (*)");
    if(dataPath.isEmpty()) return;
    QTime qt; qt.start();
    FileData data = FileData::createFromFile(dataPath);
    if(!ImageSteganographer::canEncodeData(publicImage, data, stegBits)){
        ui->logTextEdit->append(">> ERROR: Please use a larger image, and/or a smaller data size, or a larger bit size");
        return;
    }

    QString imgTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";

    ui->logTextEdit->append(QString(">> Opening data took ") + imgTime);
    qt.restart();

    privateImage = ImageSteganographer::encodeData(publicImage, data, stegBits);
    QPixmap p = QPixmap::fromImage(privateImage);
    p = p.scaled(p.size() / 4);
    privateImageLabel->setPixmap(p);
    privateImageLabel->resize(p.size() / 9);
    QString stegTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";
    ui->logTextEdit->append(QString(">> Steganography masking took ") + stegTime);
    ui->logTextEdit->append(QString(">> Opening Data took ") + QString::number(qt.elapsed()) + QString("ms"));

    statsModel->addOrSetItem("Data Loading Time", imgTime);
    statsModel->addOrSetItem("Steganography Encode: Processing Time", stegTime);

    statsModel->removeItem("Steganography Decode: Processing Time");
    statsModel->removeItem("Secondary Image Loading Time");
    statsModel->removeItem("Steganography Encode: Processing Time");
}

void MainWindow::decodeDataPushButtonClicked(){
    if(publicImage.isNull()){
        ui->logTextEdit->append(">> Unable to Decode Image, no Image loaded");
        return;
    }
    ui->logTextEdit->append(QString("Decoding Data from image... "));
    QTime qt; qt.start();

    //privateImage = ImageSteganographer::decodeData(publicImage, stegBits);
    FileData data = ImageSteganographer::decodeData(publicImage, stegBits);

    ui->logTextEdit->append(QString("Data Decoded"));
    QString decodeTime = QLocale(QLocale::English).toString(qt.elapsed()) + "ms";

    QString dataPath = QFileDialog::getExistingDirectory(this, tr("Select Save Path"),
                                                      "",
                                                      QFileDialog::ShowDirsOnly
                                                      | QFileDialog::DontResolveSymlinks);

    data.writeToFilePath(dataPath);
    ui->logTextEdit->append(QString(">> Decoding time ") + decodeTime);
    statsModel->addOrSetItem("Steganography Data Decode: Processing Time", decodeTime);
    statsModel->removeItem("Data Loading Time");
    statsModel->removeItem("Steganography Decode: Processing Time");
    statsModel->removeItem("Secondary Image Loading Time");
    statsModel->removeItem("Steganography Encode: Processing Time");
}

void MainWindow::stegBitsChanged(QString str){
    if(str == "1 bit per pixel")
        stegBits = 1;
    else if(str == "2 bits per pixel")
        stegBits = 2;
    else if(str == "3 bits per pixel")
        stegBits = 3;
    else if(str == "4 bits per pixel")
        stegBits = 4;
    if(!publicImage.isNull()){
        int sz = publicImage.size().width() * publicImage.size().height() * stegBits;
        QString x = QLocale(QLocale::English).toString(sz) + " bytes";
        QString y = QLocale(QLocale::English).toString(qMin(sz, calculateMaxDataRSA(rsaBits))) + " bytes";
        ui->logTextEdit->append(">> Steganography Limit now " + x);
        ui->logTextEdit->append(">> Overall limit " + y);
    }
}

void MainWindow::rsaBitsChanged(QString str){
    if(str == "1024 bits")
        rsaBits = 1024;
    else if(str == "2048 bits")
        rsaBits = 2048;
    else if(str == "4096 bits")
        rsaBits = 4096;
    else if(str == "8192 bits")
        rsaBits = 8192;
    if(!publicImage.isNull()){
        int sz = publicImage.size().width() * publicImage.size().height() * stegBits;
        //QString x = QLocale(QLocale::English).toString(sz) + " bytes";
        QString y = QLocale(QLocale::English).toString(qMin(sz, calculateMaxDataRSA(rsaBits))) + " bytes";
        //ui->logTextEdit->append(">> Steganography Limit now " + x);
        ui->logTextEdit->append(">> Overall limit " + y);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
