/* //////////////////////////////////////////////////////
 * ///////////// (C) nHub Nigeria Limited  //////////////
 * ///// Author: Ibrahim Timothy Onogu, August 2017 /////
 * //////////////////////////////////////////////////////
 **/

#ifndef RSA_HPP
#define RSA_HPP
#include <memory>
enum class PKCSType{ V15, V21 };

class RSA
{
public:

    enum class KeyType{ Public, Private, PublicPrivate };

    /* Creates an RSA object with the given parameters
     */
    RSA(unsigned int bits = 2048, int exponent = 65537, PKCSType type = PKCSType::V15);

    RSA(RSA&&);

    RSA& operator = (RSA&&);

    /* Creates an RSA object from the given PEM filepath
     */
    static RSA from_PEM_file(const std::string& fileName, KeyType keyType);

    /* Creates an RSA object from the given PEM string
     */
    static RSA from_PEM_string(const std::string& str, KeyType keyType);

    /* Generates a new RSA key with the given bits and exponent
     * Returns true if successful
     * */
    bool generate_new_key(unsigned int bits, int exponent);

    /* Obtains the private key's PEM string
     */
    std::string get_private_PEM() const;

    /* Obtains the public key's PEM string
     */
    std::string get_public_PEM() const;

    /* Encrypts the given string and returns encryted version
     * NOTE: RSA must hold a public key, else an exception is thrown
     */
    std::string encrypt(const std::string& str) const;

    /* Decrypts the given string and returns encryted version
     * NOTE: RSA must hold a private key, else an exception is thrown
     */
    std::string decrypt(const std::string& str) const;

    /* Returns key type
     */
    KeyType key_type() const;

    ~RSA();
private:
    class RSAImpl;
    std::unique_ptr<RSAImpl> pImpl;

    RSA(const std::string& str, KeyType KeyType);
};

#endif // RSA_HPP
