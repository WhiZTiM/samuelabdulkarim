#ifndef STATISTICSMODEL_H
#define STATISTICSMODEL_H

#include <QAbstractTableModel>
#include <QHash>

class StatisticsModel : public QAbstractTableModel
{
public:
    using QAbstractTableModel::QAbstractTableModel;
    //StatisticsModel();

    void addOrSetItem(QString key, QString value);
    void removeItem(QString key);

    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex&) const override;
    int columnCount(const QModelIndex&) const override;

private:
    QMap<QString, QString> m_items;
};

#endif // STATISTICSMODEL_H
